var express = require("express"),
    session = require("express-session"),
    passport = require("passport"),
    SpotifyStrategy = require("passport-spotify").Strategy,
    consolidate = require("consolidate"),
    fs = require('fs');

require("dotenv").config();

var port = 8888;
var authCallbackPath = "/auth/spotify/callback";

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (obj, done) {
    done(null, obj);
});

passport.use(
    new SpotifyStrategy({
            clientID: process.env.CLIENT_ID,
            clientSecret: process.env.CLIENT_SECRET,
            callbackURL: "https://livetape.io" + authCallbackPath,
        },
        function (accessToken, refreshToken, expires_in, profile, done) {
            process.nextTick(function () {
                profile.accessToken = accessToken;
                profile.refreshToken = refreshToken;
                storeUser(profile);
                return done(null, profile);
            });
        }
    )
);

var app = express();
var http = require('http').createServer(app);
app.use(express.static('static'))
var io = require('socket.io')(http);
http.listen(8888, () => {
    console.log('listening on *:8888');
});

app.set("views", __dirname + "/views");
app.set("view engine", "html");

app.use(
    session({
        secret: "keyboard cat",
        resave: true,
        saveUninitialized: true
    })
);

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(__dirname + "/public"));

app.engine("html", consolidate.nunjucks);

app.get("/", checkIfStreamer, function (req, res) {
    res.render("index.html", {
        user: req.user
    });
});

app.get("/help", function (req, res) {
    res.render("help.html");
});

app.get("/account", ensureAuthenticated, function (req, res) {
    res.render("account.html", {
        user: req.user
    });
});

app.get("/login", function (req, res) {
    res.render("login.html", {
        user: req.user
    });
});

app.get(
    "/auth/spotify",
    passport.authenticate("spotify", {
        scope: ["user-read-email", "user-read-private", "streaming", "user-read-currently-playing", "user-read-playback-state"],
        showDialog: true,
    })
);

app.get(
    authCallbackPath,
    passport.authenticate("spotify", {
        failureRedirect: "/login"
    }),
    function (req, res) {
        res.redirect("/");
    }
);

app.get("/logout", function (req, res) {
    if(req.user.id){
        fs.promises.unlink("database/user/" + req.user.id + ".json");
    }
    req.logout();
    res.redirect("/");
});

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect("/login");
}

app.get("/listen/:streamerId", ensureAuthenticated, ensureValidStreamer, function (req, res) {
    res.render("listen.html", {
        user: req.user,
        streamerId: req.params.streamerId
    });
});

async function ensureValidStreamer(req, res, next) {
    if (await isWhitelisted(req.params.streamerId)) {
        return next();
    } else {
        res.render("listen.html", {
            user: req.user
        });
    }
}

async function checkIfStreamer(req, res, next) {
    if (req.user && await isWhitelisted(req.user.id)) {
        req.user.isStreamer = true;
        if (!channels[req.user.id]) {
            initChannel(req.user.id);
        }
    }
    return next();
}

var channels = {};

function initChannel(channelName) {
    channels[channelName] = {};
    channels[channelName]['socket'] = io.of('/' + channelName);
    channels[channelName]['socket'].on('connection', socket => {
        console.log('Socket connected on channel ' + channelName);
        socket.on('input', onInput);
    });
}

async function onInput(input){
    console.log(input.data);
    var user = await getUser(input.userId);
    if(user.accessToken == input.accessToken){
        channels[input.userId]['socket'].emit('output', input.data);
    }
}

//DATABASE
async function storeUser(user) {
    await fs.promises.writeFile("database/user/" + user.id + ".json", JSON.stringify(user));
}

async function getUser(userId) {
    var user = await fs.promises.readFile("database/user/" + userId + ".json");
    return JSON.parse(user);
}

async function isWhitelisted(id) {
    var whitelist = await fs.promises.readFile("database/whitelist.json");
    whitelist = JSON.parse(whitelist);
    if (whitelist.indexOf(id) != -1) {
        return true;
    } else {
        return false;
    }
}